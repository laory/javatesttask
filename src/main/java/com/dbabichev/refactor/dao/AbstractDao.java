package com.dbabichev.refactor.dao;

import com.dbabichev.refactor.configuration.DaoConfig;
import com.dbabichev.refactor.dao.exception.DaoValidationException;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 8/22/2017
 * Time: 1:44 PM
 */
public abstract class AbstractDao {

    protected final DaoConfig daoConfig;
    private final DataSource dataSource;

    public AbstractDao(final DataSource dataSource, final DaoConfig config) {
        this.dataSource = dataSource;
        this.daoConfig = config;
    }

    protected Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    protected JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(dataSource);
    }

    void validate(long offset, long limit) {
        if (limit > daoConfig.maxLimit) {
            throw new DaoValidationException(
                    "Inefficient query. The limit is too big. Max limit: " + daoConfig.maxLimit);
        }
        if (limit < daoConfig.minLimit) {
            throw new DaoValidationException("The limit is less than " + daoConfig.minLimit);
        }
        if (offset < daoConfig.minOffset) {
            throw new DaoValidationException("The limit is less than " + daoConfig.minOffset);
        }
    }
}
