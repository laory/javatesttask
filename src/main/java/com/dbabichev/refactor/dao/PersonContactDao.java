package com.dbabichev.refactor.dao;

import com.dbabichev.refactor.model.Contact;
import com.dbabichev.refactor.model.Person;

import java.util.List;
import java.util.Optional;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/8/17
 * Time: 11:11 PM
 */
public interface PersonContactDao {

    Contact addContact(Contact contact);

    Person addPerson(Person person);

    Optional<Person> findPerson(long personId);

    Optional<Contact> findContactByPersonId(long personId);

    List<Person> getPeopleWithContacts(long offset, long limit);

    boolean checkCountryCode(long personId, String countryCode);

    long countPeopleWithContacts();

    List<String> getDistinctNames(long offset, long limit);
}
