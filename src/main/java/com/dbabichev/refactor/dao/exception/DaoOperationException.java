package com.dbabichev.refactor.dao.exception;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 8/22/2017
 * Time: 10:51 AM
 */
public class DaoOperationException extends RuntimeException {

    public DaoOperationException(String message) {
        super(message);
    }

    public DaoOperationException(String message, Throwable cause) {
        super(message, cause);
    }
}
