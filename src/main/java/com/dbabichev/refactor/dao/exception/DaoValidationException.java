package com.dbabichev.refactor.dao.exception;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 8/22/2017
 * Time: 12:11 PM
 */
public class DaoValidationException extends RuntimeException {

    public DaoValidationException(String message) {
        super(message);
    }

    public DaoValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
