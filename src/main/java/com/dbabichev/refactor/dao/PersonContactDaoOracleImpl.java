package com.dbabichev.refactor.dao;

import com.dbabichev.refactor.configuration.DaoConfig;
import com.dbabichev.refactor.dao.exception.DaoValidationException;
import com.dbabichev.refactor.model.Contact;
import com.dbabichev.refactor.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository("oraclePersonContactDao")
public class PersonContactDaoOracleImpl extends AbstractDao implements PersonContactDao {

    private static final String INSERT_CONTACT_SQL =
            "insert into CONTACT (COUNTRY_CODE, PHONE_NUMBER) values (?, ?)";
    private static final String INSERT_PERSON_SQL = "insert into PERSON (NAME, CONTACT_ID) values (?, ?)";
    private static final String FIND_PERSON_SQL =
            "select p.ID P_ID, p.NAME P_NAME, c.ID C_ID, c.COUNTRY_CODE C_COUNTRY_CODE, c.PHONE_NUMBER C_PHONE_NUMBER " +
                    "from PERSON p inner join CONTACT c on p.CONTACT_ID = c.ID " +
                    "where p.ID = ?";
    private static final String FIND_CONTACT_BY_PERSON_SQL =
            "select c.ID C_ID, c.COUNTRY_CODE C_COUNTRY_CODE, c.PHONE_NUMBER C_PHONE_NUMBER " +
                    "from PERSON p inner join CONTACT c on p.CONTACT_ID = c.ID " +
                    "where p.ID = ?";
    private static final String GET_PEOPLE_WITH_CONTACTS_SQL =
            "select p.ID P_ID, p.NAME P_NAME, c.ID C_ID, c.COUNTRY_CODE C_COUNTRY_CODE, c.PHONE_NUMBER C_PHONE_NUMBER " +
                    "from PERSON p inner join CONTACT c on p.CONTACT_ID = c.ID " +
                    "offset ? rows fetch next ? rows only";
    private static final String CHECK_COUNTRY_CODE_SQL =
            "select 1 from PERSON p inner join CONTACT c on p.CONTACT_ID = c.ID where p.ID = ? and c.COUNTRY_CODE = ?";
    private static final String COUNT_PEOPLE_WITH_CONTACTS_SQL =
            "select count(*) from PERSON p inner join CONTACT c on p.CONTACT_ID = c.ID";
    private static final String GET_DISTINCT_PERSON_NAMES_SQL =
            "select distinct p.NAME P_NAME from PERSON p inner join CONTACT c on p.CONTACT_ID = c.ID offset ? rows fetch next ? rows only";

    @Autowired
    public PersonContactDaoOracleImpl(final DataSource dataSource, final DaoConfig config) {
        super(dataSource, config);
    }

    @Override
    public Contact addContact(final Contact contact) {
        validate(contact);
        final GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        getJdbcTemplate().update(connection -> {
            final PreparedStatement statement = connection.prepareStatement(INSERT_CONTACT_SQL, new String[]{"ID"});
            statement.setString(1, contact.getCountryCode());
            statement.setString(2, contact.getPhoneNumber());
            return statement;
        }, generatedKeyHolder);
        final long contactId = generatedKeyHolder.getKey().longValue();
        return contact.cloneWith(contactId);
    }

    @Override
    public Person addPerson(final Person person) {
        validate(person);
        final GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        getJdbcTemplate().update(connection -> {
            final PreparedStatement statement = connection.prepareStatement(INSERT_PERSON_SQL, new String[]{"ID"});
            statement.setString(1, person.getName());
            statement.setLong(2, person.getContact().getId());
            return statement;
        }, generatedKeyHolder);
        final long personId = generatedKeyHolder.getKey().longValue();
        return person.cloneWith(personId);
    }

    @Override
    public Optional<Person> findPerson(final long personId) {
        try {
            final Person person = getJdbcTemplate().queryForObject(
                    FIND_PERSON_SQL,
                    new Object[]{personId},
                    (rs, rowNum) -> fetchPerson(rs)
            );
            return Optional.of(person);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Contact> findContactByPersonId(final long personId) {
        try {
            final Contact contact = getJdbcTemplate().queryForObject(
                    FIND_CONTACT_BY_PERSON_SQL,
                    new Object[]{personId},
                    (rs, rowNum) -> fetchContact(rs)
            );
            return Optional.of(contact);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Person> getPeopleWithContacts(final long offset, final long limit) {
        validate(offset, limit);
        final JdbcTemplate jdbcTemplate = getJdbcTemplate();
        jdbcTemplate.setFetchSize(daoConfig.fetchSize);
        return jdbcTemplate.query(
                GET_PEOPLE_WITH_CONTACTS_SQL,
                new Object[]{offset, limit},
                (rs, rowNum) -> fetchPerson(rs)
        );
    }

    @Override
    public boolean checkCountryCode(final long personId, final String countryCode) {
        try {
            final int existenceFlag = getJdbcTemplate().queryForObject(
                    CHECK_COUNTRY_CODE_SQL,
                    new Object[]{personId, countryCode},
                    Integer.class
            );
            return existenceFlag == 1;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    @Override
    public long countPeopleWithContacts() {
        return getJdbcTemplate().queryForObject(COUNT_PEOPLE_WITH_CONTACTS_SQL, Long.class);
    }

    @Override
    public List<String> getDistinctNames(final long offset, final long limit) {
        validate(offset, limit);
        final JdbcTemplate jdbcTemplate = getJdbcTemplate();
        jdbcTemplate.setFetchSize(daoConfig.fetchSize);
        return jdbcTemplate.queryForList(
                GET_DISTINCT_PERSON_NAMES_SQL,
                new Object[]{offset, limit},
                String.class
        );
    }

    void validate(final Contact contact) {
        if (contact == null) throw new DaoValidationException("contact is null");
        if (contact.getPhoneNumber() == null) throw new DaoValidationException("phone number is null");
        if (contact.getCountryCode() == null) throw new DaoValidationException("country code is null");
    }

    void validate(Person person) {
        if (person == null) throw new DaoValidationException("person is null");
        if (person.getName() == null) throw new DaoValidationException("person's name is null");
        validate(person.getContact());
    }

    Contact fetchContact(final ResultSet resultSet) throws SQLException {
        final long contactId = resultSet.getLong("C_ID");
        final String countryCode = resultSet.getString("C_COUNTRY_CODE");
        final String phoneNumber = resultSet.getString("C_PHONE_NUMBER");
        return new Contact(contactId, countryCode, phoneNumber);
    }

    Person fetchPerson(final ResultSet resultSet) throws SQLException {
        final long personId = resultSet.getLong("P_ID");
        final String personName = resultSet.getString("P_NAME");
        final Contact contact = fetchContact(resultSet);
        return new Person(personId, personName, contact);
    }
}
