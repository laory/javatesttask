package com.dbabichev.refactor.configuration;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/8/17
 * Time: 10:55 PM
 */
@Configuration
@PropertySource({"classpath:/com/dbabichev/refactor/db.properties", "classpath:/com/dbabichev/refactor/app.properties"})
@ComponentScan("com.dbabichev.refactor")
@EnableTransactionManagement
public class AppConfig {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean(destroyMethod = "closeConnectionPool")
    @Autowired
    public DataSource initPool(@Value("${jdbc.driver}") final String driver,
                               @Value("${jdbc.url}") final String url,
                               @Value("${jdbc.login}") final String login,
                               @Value("${jdbc.password}") final String password) {
        final BasicDataSource connectionPool = new BasicDataSource();
        connectionPool.setDriverClassName(driver);
        connectionPool.setUrl(url);
        connectionPool.setUsername(login);
        connectionPool.setPassword(password);
        return connectionPool;
    }

    @Bean
    @Autowired
    public PlatformTransactionManager txManager(final DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    @Autowired
    public DaoConfig daoConfig(@Value("${dao.fetchSize:100}") final int fetchSize,
                               @Value("${dao.maxLimit:300}") final long maxLimit,
                               @Value("${dao.minLimit:1}") final long minLimit,
                               @Value("${dao.minOffset:0}") final long minOffset) {
        return new DaoConfig(fetchSize, maxLimit, minLimit, minOffset);
    }
}
