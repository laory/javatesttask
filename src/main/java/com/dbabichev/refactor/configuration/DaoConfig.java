package com.dbabichev.refactor.configuration;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 25/8/17
 * Time: 1:47 AM
 */
public class DaoConfig {

    public final int fetchSize;
    public final long maxLimit;
    public final long minLimit;
    public final long minOffset;

    public DaoConfig(final int fetchSize, final long maxLimit, final long minLimit, final long minOffset) {
        this.fetchSize = fetchSize;
        this.maxLimit = maxLimit;
        this.minLimit = minLimit;
        this.minOffset = minOffset;
    }
}
