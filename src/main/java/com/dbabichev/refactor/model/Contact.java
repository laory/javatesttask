package com.dbabichev.refactor.model;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 8/22/2017
 * Time: 11:18 AM
 */
final public class Contact {

    private final Long id;
    private final String countryCode;
    private final String phoneNumber;

    public Contact(final String countryCode, final String phoneNumber) {
        this(null, countryCode, phoneNumber);
    }

    public Contact(final Long id, final String countryCode, final String phoneNumber) {
        this.id = id;
        this.countryCode = countryCode;
        this.phoneNumber = phoneNumber;
    }

    public Long getId() {
        return id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
        return "Contact{" + "id=" + id + ", countryCode='" + countryCode + '\'' + ", phoneNumber='" + phoneNumber +
                '\'' + '}';
    }

    public Contact cloneWith(final Long contactId) {
        return new Contact(contactId, countryCode, phoneNumber);
    }
}
