package com.dbabichev.refactor.model;

final public class Person {

    private final Long id;
    private final String name;
    private final Contact contact;

    public Person(final String name, final Contact contact) {
        this(null, name, contact);
    }

    public Person(final Long id, final String name, final Contact contact) {
        this.id = id;
        this.name = name;
        this.contact = contact;
    }

    public Person(final long id, final String name) {
        this(id, name, null);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Contact getContact() {
        return contact;
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", name='" + name + '\'' + ", contact=" + contact + '}';
    }

    public Person cloneWith(final Long personId) {
        return new Person(personId, name, contact);
    }

    public Person cloneWith(final Contact contact) {
        return new Person(id, name, contact);
    }
}
