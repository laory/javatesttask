package com.dbabichev.refactor.service;

import com.dbabichev.refactor.dao.PersonContactDao;
import com.dbabichev.refactor.model.Contact;
import com.dbabichev.refactor.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PhoneBookService {

    private final PersonContactDao personContactDao;

    @Autowired
    public PhoneBookService(@Qualifier("oraclePersonContactDao") final PersonContactDao personContactDao) {
        this.personContactDao = personContactDao;
    }

    @Transactional
    public Person addPersonContact(final String name, final String countryCode, final String phoneNumber) {
        final Contact addedContact = personContactDao.addContact(new Contact(countryCode, phoneNumber));
        return personContactDao.addPerson(new Person(name, addedContact));
    }

    public boolean hasMobile(final long personId, final String countryCode) {
        return personContactDao.checkCountryCode(personId, countryCode);
    }

    public long getSize() {
        return personContactDao.countPeopleWithContacts();
    }

    public Optional<Contact> getContact(final long personId) {
        return personContactDao.findContactByPersonId(personId);
    }

    public List<String> getNames(final long offset, final long limit) {
        return personContactDao.getDistinctNames(offset, limit);
    }

    public List<Person> getPeopleWithMobileNumbers(final long offset, final long limit) {
        return personContactDao.getPeopleWithContacts(offset, limit);
    }
}
