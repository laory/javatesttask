package com.dbabichev.refactor;

import com.dbabichev.refactor.configuration.AppConfig;
import com.dbabichev.refactor.service.PhoneBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 24/8/17
 * Time: 10:22 PM
 */
public class PhoneBookServiceStarter {

    public static void main(String[] args) {
        final ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);

        final PhoneBookService phoneBookService = ctx.getBean(PhoneBookService.class);

//        System.out.println(phoneBookService.addPersonContact("Test person", "001", "123456"));
//        System.out.println(phoneBookService.addPersonContact("Test person1", "002", "234567"));
//        System.out.println(phoneBookService.addPersonContact("Test person2", "003", "345678"));
//        System.out.println(phoneBookService.addPersonContact("Test person3", "004", "456789"));

        System.out.println("phoneBookService.getSize() = " + phoneBookService.getSize());
        System.out.println("phoneBookService.getNames(2, 2) = " + phoneBookService.getNames(2, 2));
        System.out.println("phoneBookService.getPeopleWithMobileNumbers(0, 100) = " + phoneBookService.getPeopleWithMobileNumbers(0, 100));
        System.out.println("phoneBookService.getContact(3) = " + phoneBookService.getContact(3));
        System.out.println("phoneBookService.getContact(4) = " + phoneBookService.getContact(4));
        System.out.println("phoneBookService.hasMobile(2, 002) = " + phoneBookService.hasMobile(2, "002"));
        System.out.println("phoneBookService.hasMobile(2, 012) = " + phoneBookService.hasMobile(2, "012"));
    }
}
