package com.dbabichev.threading;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Copyright 2017 Getty Images
 * User: Dmytro_Babichev
 * Date: 8/25/2017
 * Time: 9:04 PM
 */
public class PerformanceTesterImpl implements PerformanceTester {

    private static final boolean debugMode = false;

    @Override
    public PerformanceTestResult runPerformanceTest(final Runnable task, final int executionCount, final int threadPoolSize) throws InterruptedException {
        final ExecutorService executorService = Executors.newFixedThreadPool(threadPoolSize);
        try {
            final ExecutionTimeMeasurer executionTimeMeasurer = new ExecutionTimeMeasurer(executionCount, threadPoolSize, debugMode);
            final ExecutionTimeMeasurer.TimeMeasuringTask timeMeasuringTask = executionTimeMeasurer.createTimeMeasuringTask(task);

            final List<CompletableFuture<Long>> futureResults = IntStream.range(0, executionCount)
                    .mapToObj(i -> CompletableFuture.supplyAsync(timeMeasuringTask, executorService))
                    .collect(Collectors.toList());

            executionTimeMeasurer.waitUntilAllTasksAreDoneAndStopMeasurement();

            final Set<Long> executionTimeMeasurements = futureResults.stream()
                    .map(CompletableFuture::join)
                    .collect(Collectors.toSet());

            final Long minTime = executionTimeMeasurements.stream().min(Long::compare).orElse(0L);
            final Long maxTime = executionTimeMeasurements.stream().max(Long::compare).orElse(0L);

            return new PerformanceTestResult(executionTimeMeasurer.getTotalTime(), minTime, maxTime);
        } finally {
            executorService.shutdownNow();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        final PerformanceTester performanceTester = new PerformanceTesterImpl();
        final FibCalc fibCalc = new FibCalcImpl();

        final Integer n = Integer.valueOf(args[0]);
        final Integer executionCount = Integer.valueOf(args[1]);
        final Integer threadPoolSize = Integer.valueOf(args[2]);
//        final Integer n = 41;
//        final Integer executionCount = 54;
//        final Integer threadPoolSize = 43;

        if (n < 0) throw new IllegalArgumentException("n must be positive");
        if (executionCount < 1) throw new IllegalArgumentException("Execution count must be bigger than 0");
        if (threadPoolSize < 1) throw new IllegalArgumentException("Thread pool must be bigger than 0");

        System.out.println(String.format("Starting performance test with n=%s executionCount=%s threadPoolSize=%s", n, executionCount, threadPoolSize));
        final PerformanceTestResult testResult = performanceTester.runPerformanceTest(
                () -> fibCalc.fib(n),
                executionCount,
                threadPoolSize
        );
        System.out.println(testResult);
    }
}
