package com.dbabichev.threading;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.function.Supplier;

public class ExecutionTimeMeasurer {

    private final CountDownLatch taskFinishedLatch;
    private final CyclicBarrier threadsReadyCyclicBarrier;
    private final boolean debugMode;

    private volatile boolean threadsAreReady = false;
    private volatile long startTime;
    private volatile long stopTime;

    public ExecutionTimeMeasurer(final int tasks, final int threads) {
        this(tasks, threads, false);
    }

    public ExecutionTimeMeasurer(final int tasks, final int threads, final boolean debugMode) {
        taskFinishedLatch = new CountDownLatch(tasks);
        threadsReadyCyclicBarrier = new CyclicBarrier(getNumberOfThreadsToWaitFor(tasks, threads), this::startMeasurement);
        this.debugMode = debugMode;
    }

    public TimeMeasuringTask createTimeMeasuringTask(final Runnable task) {
        return () -> {
            this.waitUntilAllThreadsAreReady();

            if (debugMode) {
                System.out.println(Thread.currentThread().getName() + " - task: start");
            }

            final long startTime = System.currentTimeMillis();
            task.run();
            final long executionTime = System.currentTimeMillis() - startTime;

            if (debugMode) {
                System.out.println(Thread.currentThread().getName() + " - task: finished in " + executionTime);
            }

            this.taskIsDone();

            return executionTime;
        };
    }

    public void waitUntilAllTasksAreDoneAndStopMeasurement() {
        try {
            taskFinishedLatch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
        stopMeasurement();
    }

    public long getTotalTime() {
        return stopTime - startTime;
    }

    private int getNumberOfThreadsToWaitFor(final int tasks, final int threads) {
        return tasks < threads ? tasks : threads;
    }

    private void waitUntilAllThreadsAreReady() {
        if (threadsAreReady) return;
        if (debugMode) {
            System.out.println(Thread.currentThread().getName() + " - waitUntilAllThreadsAreReady()");
        }
        try {
            threadsReadyCyclicBarrier.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    private void taskIsDone() {
        taskFinishedLatch.countDown();
    }

    private void startMeasurement() {
        if (debugMode) {
            System.out.println(Thread.currentThread().getName() + " - measurement has been started");
        }
        threadsAreReady = true;
        startTime = System.currentTimeMillis();
    }

    private void stopMeasurement() {
        stopTime = System.currentTimeMillis();
        if (debugMode) {
            System.out.println(Thread.currentThread().getName() + " - measurement has been finished");
        }
    }

    @FunctionalInterface
    public interface TimeMeasuringTask extends Supplier<Long> {
    }
}