package com.dbabichev.threading;

/**
 * Copyright 2017 Getty Images
 * User: Dmytro_Babichev
 * Date: 8/25/2017
 * Time: 8:53 PM
 */
public class FibCalcImpl implements FibCalc {

    @Override
    public long fib(final int n) {
        if (n < 0) throw new IllegalArgumentException("Fibonacci sequence is defined only in a range n=0..+infinity");
        if (n == 0) return 0;
        if (n <= 2) return 1;
        return fib(n - 1) + fib(n - 2);
    }
}
