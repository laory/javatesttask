package com.dbabichev.cachemap;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 8/25/2017
 * Time: 10:20 AM
 */
public class ConcurrentCacheMap<K, V> implements CacheMap<K, V> {

    public static final boolean DEFAULT_REMOVED_EXPIRED_ON_READ = false;
    public static final long DEFAULT_TIME_TO_LIVE_MS = 60_000;
    public static final int MIN_TIME_TO_LIVE_MS = 0;

    private final ConcurrentHashMap<K, StampedValue> backingMap;
    private final boolean removeExpiredOnRead;
    private final Predicate<StampedValue> NOT_EXPIRED_PREDICATE = stampedValue -> !stampedValue.isExpired();

    private volatile long timeToLiveMs = DEFAULT_TIME_TO_LIVE_MS;

    public ConcurrentCacheMap() {
        backingMap = new ConcurrentHashMap<>();
        removeExpiredOnRead = DEFAULT_REMOVED_EXPIRED_ON_READ;
    }

    public ConcurrentCacheMap(final int initialCapacity, final float loadFactor, final int concurrencyLevel) {
        this(initialCapacity, loadFactor, concurrencyLevel, DEFAULT_REMOVED_EXPIRED_ON_READ);
    }

    public ConcurrentCacheMap(final int initialCapacity, final float loadFactor, final int concurrencyLevel,
                              final boolean removeExpiredOnRead) {
        backingMap = new ConcurrentHashMap<>(initialCapacity, loadFactor, concurrencyLevel);
        this.removeExpiredOnRead = removeExpiredOnRead;
    }

    @Override
    public long getTimeToLive() {
        return timeToLiveMs;
    }

    @Override
    public void setTimeToLive(final long timeToLiveMs) {
        if (timeToLiveMs <= MIN_TIME_TO_LIVE_MS) {
            throw new IllegalArgumentException("Min time to live is " + MIN_TIME_TO_LIVE_MS + " ms");
        }
        this.timeToLiveMs = timeToLiveMs;
    }

    @Override
    public V put(final K key, final V value) {
        StampedValue prevValue;
        if (value == null) {
            prevValue = backingMap.remove(key);
        } else {
            prevValue = backingMap.put(key, wrap(value));
        }
        return unwrap(prevValue);
    }

    @Override
    public void clearExpired() {
        final Map<K, StampedValue> expiredValues = backingMap.entrySet().stream()
                .filter(entry -> entry.getValue().isExpired())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        expiredValues.forEach(backingMap::remove); // we use .remove(key, oldValue) method to prevent removal of new values
    }

    @Override
    public void clear() {
        backingMap.clear();
    }

    @Override
    public boolean containsKey(final K key) {
        final StampedValue stampedValue = backingMap.get(key);
        final V value = unwrapAndRemoveIfNeeded(key, stampedValue);
        return value != null;
    }

    @Override
    public boolean containsValue(final V value) {
        return backingMap.values().stream()
                .anyMatch(stampedValue -> Objects.equals(value, stampedValue.value) && !stampedValue.isExpired());
    }

    @Override
    public V get(final K key) {
        final StampedValue stampedValue = backingMap.get(key);
        return unwrapAndRemoveIfNeeded(key, stampedValue);
    }

    @Override
    public boolean isEmpty() {
        boolean aliveValuesExist = backingMap.values().stream().anyMatch(NOT_EXPIRED_PREDICATE);
        return !aliveValuesExist;
    }

    @Override
    public V remove(final K key) {
        final StampedValue prevValue = backingMap.remove(key);
        return unwrap(prevValue);
    }

    @Override
    public int size() {
        return (int) backingMap.values().stream().filter(NOT_EXPIRED_PREDICATE).count();
    }

    private StampedValue wrap(final V value) {
        return new StampedValue(value);
    }

    private V unwrap(final StampedValue stampedValue) {
        return unwrap(stampedValue, () -> {
        });
    }

    private V unwrapAndRemoveIfNeeded(final K key, final StampedValue stampedValue) {
        return unwrap(stampedValue, () -> backingMap.remove(key, stampedValue));
    }

    private V unwrap(final StampedValue stampedValue, final Runnable expirationAction) {
        if (stampedValue == null) {
            return null;
        }
        if (stampedValue.isExpired()) {
            if (removeExpiredOnRead) {
                expirationAction.run();
            }
            return null;
        }
        return stampedValue.value;
    }

    final private class StampedValue {

        private final long timestamp;
        private final V value;

        private StampedValue(final V value) {
            this.timestamp = getTimestamp();
            this.value = value;
        }

        private long getTimestamp() {
            return Clock.getTime();
        }

        private boolean isExpired() {
            return getTimestamp() - timestamp > timeToLiveMs;
        }
    }
}
