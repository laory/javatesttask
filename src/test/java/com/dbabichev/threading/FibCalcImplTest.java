package com.dbabichev.threading;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Copyright 2017 Getty Images
 * User: Dmytro_Babichev
 * Date: 8/25/2017
 * Time: 10:51 PM
 */
@RunWith(Parameterized.class)
public class FibCalcImplTest {

    private final FibCalc fibCalc = new FibCalcImpl();

    private final int input;
    private final long expectedResult;

    @Parameters(name = "{index}: fib({0})={1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{{0, 0}, {1, 1}, {2, 1}, {3, 2}, {4, 3}, {5, 5}, {6, 8}, {23, 28657}, {40, 102334155}});
    }

    public FibCalcImplTest(final int input, final long expectedResult) {
        this.input = input;
        this.expectedResult = expectedResult;
    }

    @Test
    public void fib() {
        assertEquals(expectedResult, fibCalc.fib(input));
    }
}