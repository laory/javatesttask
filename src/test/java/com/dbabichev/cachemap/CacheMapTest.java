package com.dbabichev.cachemap;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * JUnit test case for a CacheMap implementation.
 * <p>
 * Feel free to add more methods.
 */
public class CacheMapTest {

    private CacheMap<Integer, String> cache;
    private final static long TIME_TO_LIVE = 1000;

    @Before
    public void setUp() throws Exception {
        Clock.setTime(1000);

        cache = new ConcurrentCacheMap<>();

        cache.setTimeToLive(TIME_TO_LIVE);
    }

    @Test
    public void testExpiry() throws Exception {
        cache.put(1, "apple");
        assertEquals("apple", cache.get(1));
        assertFalse(cache.isEmpty());
        Clock.setTime(3000);
        assertNull(cache.get(1));
        assertTrue(cache.isEmpty());

    }

    @Test
    public void testSize() throws Exception {
        assertEquals(0, cache.size());
        cache.put(1, "apple");
        assertEquals(1, cache.size());
        Clock.setTime(3000);
        assertEquals(0, cache.size());
    }

    @Test
    public void testPartialExpiry() throws Exception {
        //Add an apple, it will expire at 2000
        cache.put(1, "apple");
        Clock.setTime(1500);
        //Add an orange, it will expire at 2500
        cache.put(2, "orange");

        assertEquals("apple", cache.get(1));
        assertEquals("orange", cache.get(2));
        assertEquals(2, cache.size());

        //Set time to 2300 and check that only the apple has disappeared
        Clock.setTime(2300);

        assertNull(cache.get(1));
        assertEquals("orange", cache.get(2));
        assertEquals(1, cache.size());

        //Set time to 2501 and check that only the apple has disappeared
        Clock.setTime(2501);

        assertNull(cache.get(2));
        assertEquals(0, cache.size());
    }

    @Test
    public void testSetTimeToLive() throws Exception {
        //Add an apple, it will expire at 2000
        cache.put(1, "apple");
        Clock.setTime(1500);
        //Add an orange, it will expire at 2500
        cache.put(2, "orange");

        assertEquals("apple", cache.get(1));
        assertEquals("orange", cache.get(2));
        assertEquals(2, cache.size());

        //Set time to 2501 and check that only the apple has disappeared
        Clock.setTime(2501);
        assertEquals(0, cache.size());

        cache.setTimeToLive(3000);
        assertEquals(2, cache.size());
        assertEquals("apple", cache.get(1));
        assertEquals("orange", cache.get(2));
    }

    @Test
    public void testSetTimeToLive_withGetOperations() throws Exception {
        //Add an apple, it will expire at 2000
        cache.put(1, "apple");
        Clock.setTime(1500);
        //Add an orange, it will expire at 2500
        cache.put(2, "orange");

        assertEquals("apple", cache.get(1));
        assertEquals("orange", cache.get(2));
        assertEquals(2, cache.size());

        //Set time to 2501 and check that only the apple has disappeared
        Clock.setTime(2501);
        assertEquals(0, cache.size());
        assertNull(cache.get(1));
        assertNull(cache.get(2));

        cache.setTimeToLive(3000);
        assertEquals(2, cache.size());
        assertEquals("apple", cache.get(1));
        assertEquals("orange", cache.get(2));
    }

    @Test
    public void testPutReturnValue() {
        cache.put(1, "apple");
        assertEquals("apple", cache.put(1, "banana"));
        Clock.setTime(3000);
        assertNull(cache.put(1, "mango"));
    }

    @Test
    public void testPutNullReturnValue() {
        cache.put(1, "apple");
        assertEquals("apple", cache.get(1));
        assertEquals("apple", cache.put(1, null));
        assertNull(cache.get(1));
        assertEquals(0, cache.size());
        assertTrue(cache.isEmpty());
    }

    @Test
    public void testRemove() throws Exception {
        assertNull(cache.remove(new Integer(1)));

        assertNull(cache.put(new Integer(1), "apple"));

        assertEquals("apple", cache.remove(new Integer(1)));

        assertNull(cache.get(new Integer(1)));
        assertEquals(0, cache.size());
    }

    @Test
    public void testContainsKeyAndContainsValue() {
        assertFalse(cache.containsKey(1));
        assertFalse(cache.containsValue("apple"));
        assertFalse(cache.containsKey(2));
        assertFalse(cache.containsValue("orange"));

        cache.put(1, "apple");
        assertTrue(cache.containsKey(1));
        assertTrue(cache.containsValue("apple"));
        assertFalse(cache.containsKey(2));
        assertFalse(cache.containsValue("orange"));

        Clock.setTime(3000);
        assertFalse(cache.containsKey(1));
        assertFalse(cache.containsValue("apple"));
        assertFalse(cache.containsKey(2));
        assertFalse(cache.containsValue("orange"));
    }

    @Test
    public void testClear() throws Exception {
        cache.put(1, "apple");
        cache.put(2, "orange");
        cache.put(3, "pineapple");

        cache.clear();

        assertTrue(cache.isEmpty());
        assertNull(cache.get(1));
        assertNull(cache.get(2));
        assertNull(cache.get(3));
        assertEquals(0, cache.size());
    }

    @Test
    public void testClearExpired_empty() throws Exception {
        cache.put(1, "apple");
        cache.put(2, "orange");

        Clock.setTime(3000);

        cache.put(3, "pineapple");
        cache.put(4, "coconut");

        Clock.setTime(4001);

        cache.clearExpired();

        assertTrue(cache.isEmpty());
        assertNull(cache.get(1));
        assertNull(cache.get(2));
        assertNull(cache.get(3));
        assertNull(cache.get(4));
        assertEquals(0, cache.size());
    }

    @Test
    public void testClearExpired_notEmpty() throws Exception {
        cache.put(1, "apple");
        cache.put(2, "orange");

        Clock.setTime(3000);

        cache.put(3, "pineapple");
        cache.put(4, "coconut");

        Clock.setTime(4000);

        cache.clearExpired();

        assertFalse(cache.isEmpty());
        assertNull(cache.get(1));
        assertNull(cache.get(2));
        assertEquals("pineapple", cache.get(3));
        assertEquals("coconut", cache.get(4));
        assertEquals(2, cache.size());
    }
}
